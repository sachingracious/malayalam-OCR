import os
import json
import numpy as np
import pandas as pd
from tqdm import tqdm
from scipy.misc import imread, imresize


# configure
print('Reading configurations')
conf = json.load(open('conf.json', 'r'))

database = conf['database']
savepath = conf['database_save_path']
image_size = conf['image_size']
min_samples_per_label = conf['min_samples_per_label']
print('database: ', database)
print('savepath: ', savepath)
print('image_size: ', image_size)
print('min_samples_per_label: ', min_samples_per_label, type(min_samples_per_label))

image_formats = ['png', 'jpg', 'bmp']
labels = os.listdir(database)

# the fields we have to populate
target = []
features = []

#shapes = []
# walk the file paths
print('Walking the database folder')
for label in tqdm(labels):
    label_folder = os.path.join(database, label)
    files_in_label_folder = os.listdir(label_folder)

    # cycle till, min samples is filled
    while len(files_in_label_folder) < min_samples_per_label:
        files_in_label_folder = files_in_label_folder + files_in_label_folder
    # now walk the files

    for fl in tqdm(files_in_label_folder):
        if fl[-3:] in image_formats:
            fl_path = os.path.join(label_folder, fl)
            image = imread(fl_path, flatten=True)
            #shapes.append(image.shape[0] * image.shape[1])
            norm = imresize(image, image_size)
            features.append(norm.flatten())
            target.append(label)

print('Turning into Dataframe')
features = np.array(features)
df = pd.DataFrame(features)
df['target'] = target

print('Saving to file')
df = df.iloc[np.random.permutation(len(df))]
df.to_pickle(savepath)
