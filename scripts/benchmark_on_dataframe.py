import json
import pandas as pd
import numpy as np
from tqdm import tqdm
from sklearn.preprocessing import LabelEncoder
from sklearn.ensemble import RandomForestClassifier
from sklearn.cross_validation import cross_val_score


print('Reading configurations')
conf = json.load(open('conf.json', 'r'))
database = conf['database']
df_path = conf['database_save_path']
print('database: ', database)
print('dataframe path: ', df_path)


print('Reading data')
data = pd.read_pickle(df_path)
x, y = data.drop('target', axis=1), data.target

print('Encoding labels')
le  =LabelEncoder()
le.fit(y)
y_nos = le.transform(y)

print('Making classifier')
clf = RandomForestClassifier(n_jobs=-1)

print('CrossValidating')
for scoring in ['f1_weighted', 'accuracy', 'recall_weighted']:
    scores = cross_val_score(clf, x, y_nos, scoring=scoring, cv=10)
    print('{}: mean: {}std: {}'.format(scoring, scores.mean(), scores.std()))
#------------------------------------------------------------------------
